﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActualTime : MonoBehaviour
{
    /**
     * public member variable
    **/
    public Text timerLabel;

    /**
     * private script variable
    **/
    private float time;
    private System.DateTime date;
    private int hour, minutes, seconds;
   
    void Update()
    {
        //initailizing the Date (Now)
        date = System.DateTime.Now;
        hour = date.Hour;
        minutes = date.Minute;
        seconds = date.Second;

        //update the label value
        timerLabel.text = string.Format("{0:0 0} : {1:0 0} : {2:0 0}",hour, minutes, seconds);
    }
}