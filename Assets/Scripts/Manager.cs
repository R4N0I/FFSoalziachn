﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{

    public static Manager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.

    //the private member Variables for the timer;
    private float hours = 0, minutes = 2, seconds = 0;
    private string textfeld;
    private bool sound = true;

    //Draw a Gizmo to locate the controller
    /*void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "Manager", true);
    }*/

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
    public void setHours(string h)
    {
        //this.hours = (float)h;
        //parse to float value;
        this.hours = float.Parse(h);
        Debug.Log("hours set to " + h);
    }
    public float getHours()
    {
        return this.hours;
    }

    public void setMiuntes(string m)
    {
        //this.minutes = (float)m;
        this.minutes = float.Parse(m);
        Debug.Log("minutes set to " + m);
    }
    public float getMinutes()
    {
        return this.minutes;
    }

    public void setSeconds(string s)
    {
        //this.seconds = (float)s;
        this.seconds = float.Parse(s);
        Debug.Log("seconds set to " + s);
    }
    public float getSeconds()
    {
        return this.seconds;
    }

    public void setTextFeld(string s)
    {
        this.textfeld = s;
        Debug.Log(s);
    }
    public string getTextFeld()
    {
        return this.textfeld;
    }

    public void toggleSound()
    {
        this.sound = !this.sound;
    }
    public bool getSoundInfo()
    {
        return this.sound;
    }

    public void exit()
    {
        Application.Quit();
    }
}
