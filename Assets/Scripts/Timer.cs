﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    //public variables to access the variables from the outside
    public Text timerLabel;
    public Button start, stop, reset;
    public AudioSource audio;
    public AudioClip audioClip;
    //public float timeLeftHours = 0, timeLeftMinutes = 2, timeLeftSeconds = 0;

    //private variables using just in this script
    private float time;
    private float timeLeft, tleft;
    private int hour, minutes, seconds;
    private bool isRunning, playS;
    private float timeLeftHours,timeLeftMinutes, timeLeftSeconds;


    //initialize the time and set hour minutes and seconds
    void Start()
    {
        setTime();
        playS = true;
        timeLeft = timeLeftSeconds + (timeLeftMinutes * 60) + (timeLeftHours * 360);
        tleft = timeLeft;
        hour = (int)timeLeftHours;
        minutes = (int)timeLeftMinutes;
        seconds = (int)timeLeftSeconds;
        //StartCoroutine(Countdown());
    }

    void Update()
    {
        /**
         * if call just to check and set the right values to display the timer
        **/
        if (seconds < 0)
        {
            if (minutes > 0)
            {
                minutes--;
                seconds = 59;
            }
            else if ((minutes == 0) && (hour == 0))
            {
                isRunning = false;
                StopCoroutine(Countdown());
            }
            else if ((minutes <= 0) && (hour > 0))
            {
                hour--;
                minutes = 59;
                seconds = 59;
            }
        }
        if (seconds <= 0 && minutes <=0 && hour<=0)
        {
            isRunning = false;

            if (playS && Manager.instance.getSoundInfo())
            {
                playAudio();
            }
        }
        //update the label value
        timerLabel.text = string.Format("{0:0 0} : {1:0 0} : {2:0 0}", hour, minutes, seconds);

    }

    public void playAudio()
    {
        playS = false;
        audio.clip = audioClip;
        if (!audio.isPlaying)
        {
            audio.Play();
        }
        Debug.Log("audioPlayed");
    }
    public void setTime()
    {
        timeLeftHours = Manager.instance.getHours();
        Debug.Log("hour in Manager:" + Manager.instance.getHours());
        timeLeftMinutes = Manager.instance.getMinutes();
        timeLeftSeconds = Manager.instance.getSeconds();
    }
    /**
     * the public function to start the Timer
    **/
    public void startCountdown()
    {
        isRunning = true;
        StartCoroutine(Countdown());
        Debug.Log("Started");
    }

    /**
     * the public function to stop/pause the timer
    **/
    public void stopCountdown()
    {
        isRunning = false;
        StopCoroutine(Countdown());
        Debug.Log("Stopped");
        Debug.Log(Manager.instance.getSeconds());
    }


    /**
     * the publc function to stop and reset the timer to the predefined countdown Time
    **/
    public void resetCountdown()
    {
        isRunning = false;
        StopCoroutine(Countdown());
        tleft = timeLeft;
        hour = (int)timeLeftHours;
        minutes = (int)timeLeftMinutes;
        seconds = (int)timeLeftSeconds;
        playS = true;
        Debug.Log("reset");
    }

    /**
     * the coroutine which performs the countdown
    **/
    public IEnumerator Countdown()
    {
        while (isRunning)
        {
            yield return new WaitForSecondsRealtime(1.0f);
            if (isRunning)
            {
                tleft--;
                seconds--;
            }
        }
    }
}