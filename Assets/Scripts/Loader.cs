﻿using UnityEngine;
using System.Collections;

//Loads the manager prefabs at the begin of the Level
public class Loader : MonoBehaviour
{
    public GameObject GameManager;          //GameManager prefab to instantiate.


    void Awake()
    {
        //Check if a GameManager has already been assigned to static variable GameManager.instance or if it's still null
        if (Manager.instance == null)

            //Instantiate gameManager prefab
            Instantiate(GameManager);
    }

    //Draw a Gizmo to locate the controller
   /* void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "Loading", true);
    }*/

}